import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoeShop } from "./dataShoeShop";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoeShop,
    detail: dataShoeShop[0],
    cart: [],
  };
  handleChangeDetail = (value) => {
    this.setState({ detail: value });
  };
  handleBuyToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }

    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div className="container">
        <Cart cart={this.state.cart} />
        <ListShoe
          shoeArr={this.state.shoeArr}
          handleChangeDetail={this.handleChangeDetail}
          handleBuyToCart={this.handleBuyToCart}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
