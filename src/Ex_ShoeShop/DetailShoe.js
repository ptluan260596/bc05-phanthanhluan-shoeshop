import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { image, name, price, description } = this.props.detail;
    return (
      <div>
        <div className="row pt-5">
          <img src={image} alt="" className="col-4" />
          <div className="col-8">
            <h4>{name}</h4>
            <p>{price}</p>
            <p>{description}</p>
          </div>
        </div>
      </div>
    );
  }
}
